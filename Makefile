all: push

# 0.0 shouldn't clobber any released builds
TAG =0.1
PREFIX = remotejob/imgur_static

binary: server.go
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-w' -o server

container: binary
	docker build -t $(PREFIX):$(TAG) .

push: container
	docker push $(PREFIX):$(TAG)

set: 
	kubectl  --kubeconfig  ~/admin.conf set image deployment/imgur-static imgur-static=$(PREFIX):$(TAG) -n test

deploy: 
	kubectl  --kubeconfig  ~/admin.conf create -f deployment.yaml -n test    

clean:
	docker rmi -f $(PREFIX):$(TAG) || true
