//go:generate  /home/juno/neonworkspace/gowork/bin/statik -src=/home/juno/git/gitlab.com/remotejob/imgur_frontend/www

package main // import "gitlab.com/remotejob/ingur_static"

import (
	"log"
	"net/http"

	"github.com/rakyll/statik/fs"
	_ "gitlab.com/remotejob/ingur_static/statik"
)

func main() {
	statikFS, err := fs.New()
	if err != nil {
		log.Fatalf(err.Error())
	}
	http.Handle("/", http.FileServer(statikFS))

	log.Println("start on :8080 port")
	http.ListenAndServe(":8080", nil)
}
